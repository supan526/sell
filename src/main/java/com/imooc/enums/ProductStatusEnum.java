package com.imooc.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by supan on 2017/7/22.
 */
@Getter
@AllArgsConstructor
public enum  ProductStatusEnum {
    UP(0,"上线"),
    DOWN(1,"下架");
    private Integer code;
    private String message;
}
