package com.imooc.exception;

import com.imooc.enums.ExceptionEnum;
import lombok.Data;

/**
 * Created by supan on 2017/7/22.
 */
@Data
public class SellException extends RuntimeException {
    private String code;
    private String msg;
    public SellException(){
        super();
    }
    public SellException(ExceptionEnum exceptionEnum){
        super(exceptionEnum.getMsg());
        this.code = exceptionEnum.getCode();
        this.msg = exceptionEnum.getMsg();
    }

}
