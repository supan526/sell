package com.imooc.interceptor;

import com.imooc.Response.HttpResult;
import com.imooc.enums.ExceptionEnum;
import com.imooc.exception.SellException;
import lombok.extern.slf4j.Slf4j;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

/**
 * Created by supan on 2017/7/22.
 */
@Slf4j
public class SellExceptionInterceptor implements MethodInterceptor {
    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {

        String location = methodInvocation.getMethod().getDeclaringClass().getName() + "." +
                methodInvocation.getMethod().getName() + "()";
        try {
            //// TODO: 2017/7/22 执行校验的逻辑
            // 执行controller
            Object proceed = methodInvocation.proceed();
            return proceed;
        } catch (Exception e) {
            if(e instanceof SellException){
                if(log.isDebugEnabled()){
                    log.debug(location+e.getMessage(),e);
                }
                return HttpResult.fail((SellException) e);
            }else{
                log.error(location + e.getMessage(), e);
                return HttpResult.fail(new SellException(ExceptionEnum.PARAM_ERROR));
            }
        }
    }
}
