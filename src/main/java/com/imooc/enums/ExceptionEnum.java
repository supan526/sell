package com.imooc.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by supan on 2017/7/22.
 */
@Getter
@AllArgsConstructor
public enum ExceptionEnum {
    PARAM_ERROR("param.error","参数错误"),
    SYSTEM_ERROR("system.error","系统错误");
    //异常代码
    private String code;
    //异常信息
    private String msg;

}
