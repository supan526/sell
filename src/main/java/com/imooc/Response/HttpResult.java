package com.imooc.Response;

import com.imooc.enums.ResponseEnum;
import com.imooc.exception.SellException;
import lombok.Builder;
import lombok.ToString;

/**
 * Created by supan on 2017/7/22.
 */
@Builder
@ToString
public class HttpResult<T> extends BaseVo {
    private String code;
    private String message;
    private T data;

    public static<T> HttpResult ok(T data){
        return HttpResult.builder()
                .code(ResponseEnum.SUCCESS.getCode())
                .message(ResponseEnum.SUCCESS.getMessage())
                .data(data)
                .build();
    }
    public static<T> HttpResult ok(){
        return HttpResult.builder()
                .code(ResponseEnum.SUCCESS.getCode())
                .message(ResponseEnum.SUCCESS.getMessage())
                .build();
    }
    public static HttpResult fail(){
        return HttpResult.builder()
                .code(ResponseEnum.FAIL.getCode())
                .message(ResponseEnum.FAIL.getMessage())
                .build();
    }
    public static HttpResult fail(SellException sellException){
        return HttpResult.builder()
                .code(sellException.getCode())
                .message(sellException.getMessage())
                .build();
    }
}
