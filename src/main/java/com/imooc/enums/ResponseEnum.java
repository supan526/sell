package com.imooc.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by supan on 2017/7/22.
 */
@Getter
@AllArgsConstructor
public enum  ResponseEnum {
    SUCCESS("0","成功"),
    FAIL("1","失败");
    private String code;
    private String message;
}
